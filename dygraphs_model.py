# -*- coding: utf-8 -*-

import re
import os
import MySQLdb
import shutil
import pickle

import base_model

from trac.util import Markup

import sys
reload(sys)
sys.setdefaultencoding('utf-8')
#line-labels

class DyGraph(object):
    @classmethod   
    def get_cell_resel_dygraph_js(self, anntxt, Data1, Data2):   
        if Data2 == '':
            Data2 = "Date,Temperature\n"
            
        __LittleToken__ = Markup('<')  

        __DoubleQToken__ = Markup('"') 
        
        __ZeroToken__ = Markup('"0"')  
        __DoubleZeroToken__ = Markup('"00"')

        __mousewheel__ = Markup('"mousewheel"')
        __click__ = Markup('"click"')
        __DOMMouseScroll__ = Markup('"DOMMouseScroll"')

        __html__ = Markup('"<span id=\'" + name + "\'>" +name+ ": " +(ann[i].shortText || \'(icon)\')+ " , " +ann[i].text+ "</span><br/>";')
        
        char_javascript = ''' 
var req = new XMLHttpRequest();  //get annotation file
req.onreadystatechange = function () {
  if (req.readyState == 4) {
    if (req.status === 200 || // Normal http
        req.status === 0) { // Chrome w/ --allow-file-access-from-files
      var data = req.responseText;
      //alert('req: drawGraph')
      drawGraph(data);
    }
  }
};
req.open('GET', ' '''+anntxt+''' ', true);
req.send(null);

var formatMS = function(total_MS) {			//convert milliseconds to arm time(h:m:s:ms)
	var hours = Math.floor(total_MS/1000/3600);
	var minutes = Math.floor((total_MS/1000/60 - (hours*60)));
	var seconds = Math.floor((total_MS/1000 - (hours*3600) - (minutes*60)));
	var milliseconds = Math.round((total_MS - (hours*3600*1000) - (minutes*60*1000) - (seconds*1000)));
	if (hours   '''+__LittleToken__+''' 10) {hours   = '''+__ZeroToken__+'''+hours;} 
	if (minutes '''+__LittleToken__+''' 10) {minutes = '''+__ZeroToken__+'''+minutes;} 
	if (seconds '''+__LittleToken__+''' 10) {seconds = '''+__ZeroToken__+'''+seconds;}
	if (milliseconds '''+__LittleToken__+''' 10) {milliseconds = '''+__DoubleZeroToken__+'''+milliseconds;}
	if (milliseconds '''+__LittleToken__+''' 100) {milliseconds = '''+__ZeroToken__+'''+milliseconds;}
	var time = hours+':'+minutes+':'+seconds+':'+milliseconds; 
	return time;
};
	    
Dygraph.addEvent(document, '''+__mousewheel__+''', function() { lastClickedGraph = null; });
Dygraph.addEvent(document, '''+__click__+''', function() { lastClickedGraph = null; });
Dygraph.addEvent(document, '''+__DOMMouseScroll__+''', function() { lastClickedGraph = null; });  //for firefox mouse scroll,the event is different from chrome

//draw the first graph
g1 = new Dygraph(
    document.getElementById('''+__DoubleQToken__+'''graphdiv1'''+__DoubleQToken__+'''),
    '''+__DoubleQToken__+Data1+__DoubleQToken__+''', 
    {
    	width:900,//1200
     	height:600,
     	title: 'TD Cell RSCP',
      	titleHeight: 32,
      	ylabel: 'RSCP',
      	xlabel: 'ARM Time',
    	drawPoints:true,
    	xRangePad:100,
    	connectSeparatedPoints: true,
		hideOverlayOnMouseOut:false,
    	labelsDiv: document.getElementById('''+__DoubleQToken__+'''status1'''+__DoubleQToken__+'''),
		labelsShowZeroValues:false,
    	labelsSeparateLines: true,
		highlightCircleSize: 2,
		strokeWidth: 1,
		strokeBorderWidth: 1,
		highlightSeriesOpts: {
			strokeWidth: 3,
			strokeBorderWidth: 1,
			highlightCircleSize: 5,
		},
		interactionModel : {
			'mousedown' :  function (event, g, context) { Dygraph.defaultInteractionModel.mousedown(event, g, context); },
			'mousemove' : function (event, g, context) {Dygraph.defaultInteractionModel.mousemove(event, g, context);},
			'mouseup' : upV3,
			'click' : clickV3,
			'dblclick' : dblClickV3,
			'mousewheel' : scrollV3,
			'DOMMouseScroll':scrollV3
		},
    	axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       }
      	},
		drawCallback: function(dg,is_initial) {  //show serve cell line red and secondary cell line blue
            //alert('drawCallback');
            //alert(is_initial);
			if (!is_initial )  
				return;
		    //alert('colorValue');
			//var colorArry = colorValue();
			
	        var colorArry = new Array();
	        //alert('colorArry');
	        var end = dg.indexFromSetName('''+__DoubleQToken__+'''ServCell'''+__DoubleQToken__+''')-1;
	        //alert('end: '+end);
	        for (var i = 0; i '''+__LittleToken__+''' end; ++i) {
		        colorArry.push('black');	
	        }
	        colorArry.push('red');
	        colorArry.push('blue');
	        //return colorArry;
			
			dg.updateOptions({colors: colorArry});
		}
    }
);
 
var onclick = function(ev) {
    if (g1.isSeriesLocked()) {
      g1.clearSelection();
    } else {
      g1.setSelection(g1.getSelection, g1.getHighlightSeries(), true);
    }
};
g1.updateOptions( {clickCallback: onclick},true);

function colorValue() {
    //alert('test');
	var color = new Array();
	alert('start');
	var end = g1.indexFromSetName('''+__DoubleQToken__+'''ServCell'''+__DoubleQToken__+''')-1;
	//alert('end');
	//alert(end);
	for (var i = 0; i '''+__LittleToken__+''' end; ++i) {
		color.push('black');
	}
	color.push('red');
	color.push('blue');
	return color;
}

//show the chart in the user entered value range
var valRange = new Array();
function myFunction() {
	valRange = [];
	var x=document.getElementById('''+__DoubleQToken__+'''minValue'''+__DoubleQToken__+''').value;
	valRange.push(parseFloat(x));
	
	var y=document.getElementById('''+__DoubleQToken__+'''maxValue'''+__DoubleQToken__+''').value;
	valRange.push(parseFloat(y));
	g1.updateOptions({valueRange: valRange});
}	

var ser = 0;
var sec_ser = 0;
function change1(el) {
	var num = g1.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!ser) {
			ser = num - 1;
		}
	}
	g1.setVisibility(ser, el.checked);
}

function change2(el) {
	var num = g1.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!sec_ser) {
			sec_ser = num - 1;
		}
	}
	g1.setVisibility(sec_ser, el.checked);
}


// draw the second picture	  
g2 = new Dygraph(
    document.getElementById('''+__DoubleQToken__+'''graphdiv2'''+__DoubleQToken__+'''),
    '''+Data2+''',  		
	{
	    width:900,//1200
		height:100,
     	title: 'TD Cell Reselection',
      	ylabel: 'Status',
      	xlabel: 'ARM Time',     	
		drawYAxis:true,
		drawXGrid:false,
		xRangePad:100,
		labelsDiv: document.getElementById('''+__DoubleQToken__+'''status2'''+__DoubleQToken__+'''),
		labelsShowZeroValues:false,
		hideOverlayOnMouseOut:false,
	    axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       },
		  y: {
			ticker: function(min, max, pixels, opts, dygraph, vals) {
				return [{v:0, label:'''+__DoubleQToken__+'''0'''+__DoubleQToken__+'''}];
			}
		  }
      	}
	}
);

//add the annotations
var drawGraph = function (data) {
    //alert(data);
    var lines = data.split('''+__DoubleQToken__+'''\\n'''+__DoubleQToken__+''');    
    var annotations = [];
    
    var num = Math.round(lines.length/4);   //may have blank line at the end
    //alert('num='+num);
    
    for (var idx = 0; idx '''+__LittleToken__+''' num; idx++) {
    	var arry = [];
    	//alert('idx='+idx);
    	
    	for (var j = 0; j '''+__LittleToken__+''' 4; j++) {
    		var i = 4*idx+j;
            var line = lines[i];
            // Oftentimes there's a blank line at the end. Ignore it.
            if (line.length == 0) {
                continue;
            }
            var row = line.split('''+__DoubleQToken__+''':'''+__DoubleQToken__+''');
            row[1] = row[1].replace(/^\s*|\s*$/g, ''); //remove begin and end spaces
            arry.push(row[1]);
        }
        
    	var annFile = {
			series: arry[0],
			x: arry[1],
			shortText: arry[2],
			text: arry[3]
    	};
		if (annFile.shortText == '''+__DoubleQToken__+'''rej'''+__DoubleQToken__+''') {
			//annFile.cssClass = 'annRej';
			annFile.icon = 'http://tracsrv/trac/ilog/chrome/hw/images/frown_face.png';
			annFile.width = 20;
			annFile.height = 20;
		} else {
			//annFile.cssClass = 'annSuc';
			annFile.icon = '/trac/ilog/chrome/hw/images/smiley_face.png';
			annFile.width = 20;
			annFile.height = 20;
		}
		//alert(idx+': annFile='+annFile.text+' '+annFile.icon);
    	annotations.push(annFile);
	}
	//alert('1annotations='+annotations);
    {
                //var ann = g2.annotations();
                var ann = annotations;
                //alert(ann);

                
                var html = '';
                //alert('ann.length='+ann.length);
                
                for (var i = 0; i '''+__LittleToken__+''' ann.length; i++) {
                    var name = nameAnnotation(ann[i]);
                    html += '''+__html__+'''
                    //html += "<span id='" + name + "'>"
                    //html += name + ": " + (ann[i].shortText || '(icon)')
                    //html += " , " + ann[i].text + "</span><br/>";
                }
                //alert(html);
                document.getElementById('''+__DoubleQToken__+'''list'''+__DoubleQToken__+''').innerHTML = html;
    }	
	g2.setAnnotations(annotations);
	//alert('2annotations='+annotations);
		
    function nameAnnotation(ann) {
        return '''+__DoubleQToken__+'''('''+__DoubleQToken__+''' + ann.series + '''+__DoubleQToken__+''', '''+__DoubleQToken__+''' + formatMS(ann.x) + '''+__DoubleQToken__+''')'''+__DoubleQToken__+''';
    }
	
	var saveBg = '';
	g2.updateOptions(
	{
		drawCallback: function (dg, is_initial) {
		    //alert('drawCallback');
		    //alert(is_initial);
            if (!is_initial) 
            {
                //var ann = g2.annotations();
                var ann = annotations;
                //alert(ann);                
                var html = '';
                //alert('ann.length='+ann.length);
                
                for (var i = 0; i '''+__LittleToken__+''' ann.length; i++) {
                    var name = nameAnnotation(ann[i]);
                    html += '''+__html__+'''
                }
                //alert(html);
                document.getElementById('''+__DoubleQToken__+'''list'''+__DoubleQToken__+''').innerHTML = html;
                
                return;
            }
            g2.setAnnotations(annotations);
        },
		annotationMouseOverHandler: function(ann, point, dg, event) {
		  document.getElementById(nameAnnotation(ann)).style.fontWeight = 'bold';
		  saveBg = ann.div.style.backgroundColor;
		  ann.div.style.backgroundColor = '#ddd';
		},
		annotationMouseOutHandler: function(ann, point, dg, event) {
		  document.getElementById(nameAnnotation(ann)).style.fontWeight = 'normal';
		  ann.div.style.backgroundColor = saveBg;
		}

	},true );
}
        '''   

       
        return char_javascript



    @classmethod   
    def get_dygraph_js(self, VarName, ContainerName, Data):
            
        char_javascript = '''  
            __VarName__ = new Dygraph(
                document.getElementById("__ContainerName__"),
                __Data__,
                {
                }
            );
            
        '''
        
        char_javascript = char_javascript.replace('__ContainerName__',ContainerName) 
        char_javascript = char_javascript.replace('__VarName__',VarName)  
        char_javascript = char_javascript.replace('__Data__',Data) 
        #char_javascript = char_javascript.replace('__width__',width) 
        #char_javascript = char_javascript.replace('__height__',height)
        #char_javascript = char_javascript.replace('__title__',title)
        
        
        return char_javascript
        
    @classmethod   
    def get_dygraph_rscp_js(self, VarName, ContainerName, Data):
            
        char_javascript = '''  
            __VarName__ = new Dygraph(
                document.getElementById("__ContainerName__"),
                __Data__,
                {
    	width:1200,
     	height:600,
     	title: 'TD Cell Reselection',
      	titleHeight: 32,
      	ylabel: 'RSCP',
      	xlabel: 'ARM Time',
    	drawPoints:true,
    	xRangePad:100,
    	connectSeparatedPoints: true,
		hideOverlayOnMouseOut:false,
    	labelsDiv: document.getElementById("status1"),
		labelsShowZeroValues:false,
    	labelsSeparateLines: true,
		highlightCircleSize: 2,
		strokeWidth: 1,
		strokeBorderWidth: 1,
		highlightSeriesOpts: {
			strokeWidth: 3,
			strokeBorderWidth: 1,
			highlightCircleSize: 5,
		},
		interactionModel : {
			'mousedown' :  function (event, g, context) { Dygraph.defaultInteractionModel.mousedown(event, g, context); },
			'mousemove' : function (event, g, context) {Dygraph.defaultInteractionModel.mousemove(event, g, context);},
			'mouseup' : upV3,
			'click' : clickV3,
			'dblclick' : dblClickV3,
			'mousewheel' : scrollV3,
			'DOMMouseScroll':scrollV3
		},
    	axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       }
      	},
		drawCallback: function(dg,is_initial) {  //show serve cell line red and secondary cell line blue
			if (!is_initial )  
				return;
			//var colorArry = colorValue();
			
	        var colorArry = new Array();	
	//var end = __VarName__.indexFromSetName("ServCell")-1;
	var end=5;
	for (var i = 0; ; ++i) {
	    if ( i == end )
	        break;
		colorArry.push('black');	
	}
	colorArry.push('red');
	colorArry.push('blue');

	
			dg.updateOptions({colors: colorArry});
		}
                }
            );
            
var onclick = function(ev) {
    if (__VarName__.isSeriesLocked()) {
      __VarName__.clearSelection();
    } else {
      __VarName__.setSelection(__VarName__.getSelection, __VarName__.getHighlightSeries(), true);
    }
};
__VarName__.updateOptions( {clickCallback: onclick},true);

function colorValue1() {
	var color = new Array();	
	var end = __VarName__.indexFromSetName("ServCell")-1;
	for (var i = 0; ; ++i) {
	    if ( i == end )
	        break;
		color.push('black');	
	}
	color.push('red');
	color.push('blue');
	return color;
}            


        '''
        
        char_javascript = char_javascript.replace('__ContainerName__',ContainerName) 
        char_javascript = char_javascript.replace('__VarName__',VarName)  
        char_javascript = char_javascript.replace('__Data__',Data) 
        #char_javascript = char_javascript.replace('__width__',width) 
        #char_javascript = char_javascript.replace('__height__',height)
        #char_javascript = char_javascript.replace('__title__',title)
        
        
        return char_javascript
        
    @classmethod   
    def get_dygraph_cellresel_js(self, VarName, ContainerName, Data):
            
        char_javascript = '''  
            __VarName__ = new Dygraph(
                document.getElementById("__ContainerName__"),
                __Data__,
                {
	    width:1200,
		height:100,
		drawYAxis:true,
		drawXGrid:false,
		xRangePad:100,
		labelsDiv: document.getElementById("status2"),
		labelsShowZeroValues:false,
		hideOverlayOnMouseOut:false,
	    axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       },
		  y: {
			ticker: function(min, max, pixels, opts, dygraph, vals) {
				return [{v:0, label:"0"}];
			}
		  }
      	}
	}
            );


var formatMS = function(total_MS) {			//convert milliseconds to arm time(h:m:s:ms)
	var hours = Math.floor(total_MS/1000/3600);
	var minutes = Math.floor((total_MS/1000/60 - (hours*60)));
	var seconds = Math.floor((total_MS/1000 - (hours*3600) - (minutes*60)));
	var milliseconds = Math.round((total_MS - (hours*3600*1000) - (minutes*60*1000) - (seconds*1000)));
	//if (hours   < 10) {hours   = "0"+hours;} 
	//if (minutes < 10) {minutes = "0"+minutes;} 
	//if (seconds < 10) {seconds = "0"+seconds;}
	//if (milliseconds < 10) {milliseconds = "00"+milliseconds;}
	//if (milliseconds < 100) {milliseconds = "0"+milliseconds;}
	var time = hours+':'+minutes+':'+seconds+':'+milliseconds; 
	return time;
};         



        '''
        
        char_javascript = char_javascript.replace('__ContainerName__',ContainerName) 
        char_javascript = char_javascript.replace('__VarName__',VarName)  
        char_javascript = char_javascript.replace('__Data__',Data) 
        #char_javascript = char_javascript.replace('__width__',width) 
        #char_javascript = char_javascript.replace('__height__',height)
        #char_javascript = char_javascript.replace('__title__',title)
        
        
        return char_javascript
        
    @classmethod   
    def get_dygraph_cellresel_ajax_js(self):
            
        char_javascript = '''  
var req = new XMLHttpRequest();  //get annotation file
req.onreadystatechange = function () {
  if (req.readyState == 4) {
    if (req.status === 200 || // Normal http
        req.status === 0) { // Chrome w/ --allow-file-access-from-files
      var data = req.responseText;
      drawGraph(data);
    }
  }
};
req.open('GET', 'ann.txt', true);
req.send(null);
        '''        
        return char_javascript


    @classmethod   
    def get_BoundFunc_js(self, dygraph_VarName):
        char_javascript = '''  
            //show the chart in the user entered value range
            var valRange = new Array();
            function BoundFunc() {
            	valRange = [];
            	var x=document.getElementById("minValue").value;
            	valRange.push(parseFloat(x));
	
            	var y=document.getElementById("maxValue").value;
            	valRange.push(parseFloat(y));
            	__dygraph_VarName__.updateOptions({valueRange: valRange});
            }	
        '''
        
        char_javascript = char_javascript.replace('__dygraph_VarName__',dygraph_VarName) 
        #char_javascript = char_javascript.replace('__VarName__',VarName)  
        #char_javascript = char_javascript.replace('__Data__',Data)         
        return char_javascript

    @classmethod   
    def get_ChangeCellColor_js(self, dygraph_VarName):
        char_javascript = '''  
var ser = 0;
var sec_ser = 0;
function change1(el) {
	var num = __dygraph_VarName__.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!ser) {
			ser = num - 1;
		}
	}
	__dygraph_VarName__.setVisibility(ser, el.checked);
}

function change2(el) {
	var num = __dygraph_VarName__.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!sec_ser) {
			sec_ser = num - 1;
		}
	}
	__dygraph_VarName__.setVisibility(sec_ser, el.checked);
}
        '''
        
        char_javascript = char_javascript.replace('__dygraph_VarName__',dygraph_VarName) 
        #char_javascript = char_javascript.replace('__VarName__',VarName)  
        #char_javascript = char_javascript.replace('__Data__',Data)         
        return char_javascript
        
    @classmethod
    def get_dygraph_data2(self, cvs_file):
        #sample
             
        import csv_action    
        dygraph_data = ''        
        csv_data = csv_action.csv_action_read(cvs_file)
        
        line_no = 0
        line_len = 0
        line_len = len(csv_data)
        for line in csv_data:
            line_no += 1            
            dygraph_data += Markup('"')+line+'\\n'+Markup('"')                 
            if line_no < line_len:
                dygraph_data += ' + \n'
            
        return dygraph_data

    @classmethod
    def get_dygraph_data(self, cvs_file):
        import os
        import sys  
        import csv  
             
        csvFile = csv.reader(open(cvs_file))     
        
        dygraph_data = ''                
        #line_no = 0
        #line_len = 0
        
        #line_len = len(csv_data)

        #    "Date,Temperature\n2008-05-07,75\n2008-05-08,70\n2008-05-09,80\n"
        dygraph_data +=''
        for line in csvFile:         
            line_str = ''
            data_len = len(line)
            data_no = 0
            for data in line:  
                data_no += 1
                if data_no < data_len:
                    line_str += data+','            
                else:
                    line_str += data
        
            #line_no += 1            
            dygraph_data += line_str+'\\n'      
            #if line_no < line_len:
            #    dygraph_data += ' + \n'
        dygraph_data +=''            
        return dygraph_data

    @classmethod
    def get_logdata_base_url(self):             
        import ilog_web_ui        
            
        return ilog_web_ui.gl_log_baseurl
    
