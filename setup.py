#!/usr/bin/env python

from setuptools import setup, find_packages

PACKAGE = 'iLog'
VERSION = '0.1'


setup(
    name=PACKAGE, version=VERSION,
    description="A plugin for Log Graph",
    packages=find_packages(exclude=['ez_setup', '*.tests*']),
    package_data={
        'iLog': ['templates/*.html', 
                                 'htdocs/css/*.css',
                                 'htdocs/js/*.js',
                                 'htdocs/images/*',
                                 'htdocs/log/*']
    },
    entry_points = {
        'trac.plugins': [
            'iLog.web_ui = iLog.web_ui',
        ]
    }
)