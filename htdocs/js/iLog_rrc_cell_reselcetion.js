


var req = new XMLHttpRequest();  //get annotation file
req.onreadystatechange = function () {
  if (req.readyState == 4) {
    if (req.status === 200 || // Normal http
        req.status === 0) { // Chrome w/ --allow-file-access-from-files
      var data = req.responseText;
	  
      drawGraph(data);
    }
  }
};
req.open('GET', 'ann.txt', true);
req.send(null);

var formatMS = function(total_MS) {			//convert milliseconds to arm time(h:m:s:ms)
	var hours = Math.floor(total_MS/1000/3600);
	var minutes = Math.floor((total_MS/1000/60 - (hours*60)));
	var seconds = Math.floor((total_MS/1000 - (hours*3600) - (minutes*60)));
	var milliseconds = Math.round((total_MS - (hours*3600*1000) - (minutes*60*1000) - (seconds*1000)));
	if (hours   < 10) {hours   = "0"+hours;} 
	if (minutes < 10) {minutes = "0"+minutes;} 
	if (seconds < 10) {seconds = "0"+seconds;}
	if (milliseconds < 10) {milliseconds = "00"+milliseconds;}
	if (milliseconds < 100) {milliseconds = "0"+milliseconds;}
	var time = hours+':'+minutes+':'+seconds+':'+milliseconds; 
	return time;
};

Dygraph.addEvent(document, "mousewheel", function() { lastClickedGraph = null; });
Dygraph.addEvent(document, "click", function() { lastClickedGraph = null; });
Dygraph.addEvent(document, "DOMMouseScroll", function() { lastClickedGraph = null; });  //for firefox mouse scroll,the event is different from chrome

//draw the first graph
g1 = new Dygraph(
    document.getElementById("graphdiv1"),
    "data.csv",  		//http://localhost:8080/data.csv
    {
    	width:1000,
     	height:600,
     	title: 'TD Cell Reselection',
      	titleHeight: 32,
      	ylabel: 'RSCP',
      	xlabel: 'ARM Time',
    	drawPoints:true,
    	xRangePad:100,
    	connectSeparatedPoints: true,
		hideOverlayOnMouseOut:false,
    	labelsDiv: document.getElementById("status1"),
		labelsShowZeroValues:false,
    	labelsSeparateLines: true,
		highlightCircleSize: 2,
		strokeWidth: 1,
		strokeBorderWidth: 1,
		highlightSeriesOpts: {
			strokeWidth: 3,
			strokeBorderWidth: 1,
			highlightCircleSize: 5,
		},
		interactionModel : {
			'mousedown' :  function (event, g, context) { Dygraph.defaultInteractionModel.mousedown(event, g, context); },
			'mousemove' : function (event, g, context) {Dygraph.defaultInteractionModel.mousemove(event, g, context);},
			'mouseup' : upV3,
			'click' : clickV3,
			'dblclick' : dblClickV3,
			'mousewheel' : scrollV3,
			'DOMMouseScroll':scrollV3
		},
    	axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       }
      	},
		drawCallback: function(dg,is_initial) {  //show serve cell line red and secondary cell line blue
			if (!is_initial )  
				return;
			var colorArry = colorValue();
			dg.updateOptions({colors: colorArry});
		}
    }
  );
 
 var onclick = function(ev) {
    if (g1.isSeriesLocked()) {
      g1.clearSelection();
    } else {
      g1.setSelection(g1.getSelection, g1.getHighlightSeries(), true);
    }
};
g1.updateOptions( {clickCallback: onclick},true);


function colorValue() {
	var color = new Array();
	var end = g1.indexFromSetName("ServCell")-1;
	for (var i = 0; i < end; ++i) {
		color.push('black');	
	}
	color.push('red');
	color.push('blue');
	return color;
}

//show the chart in the user entered value range
var valRange = new Array();
function myFunction() {
	valRange = [];
	var x=document.getElementById("minValue").value;
	valRange.push(parseFloat(x));
	
	var y=document.getElementById("maxValue").value;
	valRange.push(parseFloat(y));
	g1.updateOptions({valueRange: valRange});
}	

var ser = 0;
var sec_ser = 0;
function change1(el) {
	var num = g1.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!ser) {
			ser = num - 1;
		}
	}
	g1.setVisibility(ser, el.checked);
}

function change2(el) {
	var num = g1.indexFromSetName(el.id);
	if (isNaN(num)) {
	} else {
		if (!sec_ser) {
			sec_ser = num - 1;
		}
	}
	g1.setVisibility(sec_ser, el.checked);
}


// draw the second picture	  
g2 = new Dygraph(
    document.getElementById("graphdiv2"),
    "annfile.csv",  		//http://localhost:8080/data.csv
	{
	    width:1200,
		height:100,
		drawYAxis:true,
		drawXGrid:false,
		xRangePad:100,
		labelsDiv: document.getElementById("status2"),
		labelsShowZeroValues:false,
		hideOverlayOnMouseOut:false,
	    axes: {
	      x: {
	         valueFormatter : formatMS,
	         axisLabelFormatter : formatMS
	       },
		  y: {
			ticker: function(min, max, pixels, opts, dygraph, vals) {
				return [{v:0, label:"0"}];
			}
		  }
      	}
	}
);

//add the annotations
var drawGraph = function (data) {
    var lines = data.split("\n");
    var annotations = [];
    var num = Math.round(lines.length/4);   //may have blank line at the end
    for (var idx = 0; idx < num; idx++) {
    	var arry = [];
    	for (var j = 0; j < 4; j++) {
    		var i = 4*idx+j;
        var line = lines[i];
        // Oftentimes there's a blank line at the end. Ignore it.
        if (line.length == 0) {
            continue;
        }
        var row = line.split(":");
        row[1] = row[1].replace(/^\s*|\s*$/g, ''); //remove begin and end spaces
        arry.push(row[1]);
      }
    	var annFile = {
			series: arry[0],
			x: arry[1],
			shortText: arry[2],
			text: arry[3]
    	};
		if (annFile.shortText == "rej") {
			//annFile.cssClass = 'annRej';
			annFile.icon = 'frown_face.png';
			annFile.width = 20;
			annFile.height = 20;
		} else {
			//annFile.cssClass = 'annSuc';
			annFile.icon = 'smiley_face.png';
			annFile.width = 20;
			annFile.height = 20;
		}
    	annotations.push(annFile);
	}
		
    function nameAnnotation(ann) {
        return "(" + ann.series + ", " + formatMS(ann.x) + ")";
    }
	
	var saveBg = '';
	g2.updateOptions(
	{
		drawCallback: function (g2, is_initial) {
            if (!is_initial) {
                var ann = g2.annotations();
                var html = "";
                for (var i = 0; i < ann.length; i++) {
                    var name = nameAnnotation(ann[i]);
                    html += "<span id='" + name + "'>"
                    html += name + ": " + (ann[i].shortText || '(icon)')
                    html += " , " + ann[i].text + "</span><br/>";
                }
                document.getElementById("list").innerHTML = html;
                return;
            }
            g2.setAnnotations(annotations);
        },
		annotationMouseOverHandler: function(ann, point, dg, event) {
		  document.getElementById(nameAnnotation(ann)).style.fontWeight = 'bold';
		  saveBg = ann.div.style.backgroundColor;
		  ann.div.style.backgroundColor = '#ddd';
		},
		annotationMouseOutHandler: function(ann, point, dg, event) {
		  document.getElementById(nameAnnotation(ann)).style.fontWeight = 'normal';
		  ann.div.style.backgroundColor = saveBg;
		}

	},true );
}

