# -*- coding: utf-8 -*-
#
import os

import web_utils

gl_Input = 'Input'
gl_Load = 'Load'

gl_Ower = 'Ower'
gl_LogelFile = 'LogelFile'
gl_ParseLibs = 'ParseLibs'

gl_RSCP = 'RSCP'
gl_CellResel = 'CellResel'
gl_CellResel_Txt = 'CellResel_Txt'
gl_Win_LogPath = '\\\\shnas1\\To_Internal\\'
gl_Linux_LogPath = '/home/share/Log/'
#gl_Linux_Txt_LogPath = '/iTest/chrome/hw/log/'
gl_Linux_Txt_LogPath = '/iTest/chrome/site/'
#gl_Egg_Txt_LogPath = '/usr/lib/python2.4/site-packages/iLog-0.1-py2.4.egg/iLog/htdocs/log'
gl_Egg_Txt_LogPath = '/trac/Projects/iTest/htdocs'

gl_base_url = 'http://tracsrv/ilog/RRC_CellReselcetion/log'
gl_sample_url = 'http://tracsrv/chrome/site/Log/Sample/data.csv'
gl_firefox_url = 'http://tracsrv/chrome/site/Mozilla Firefox.rar'
gl_log_baseurl = 'http://tracsrv/chrome/site/Log/'
gl_LogPath = 'D:\\site\\ilog\\htdocs\\Log\\'

gl_LogToolsPath = 'D:\\Log\\Tools\\'

gl_ilog_fields = ["id","Ower", "LogelFile", "ParseLibs", "Status"]  



def ilog_rrc_cell_reselcetion(env, req, db_conn): 
        js = '' 

        sid = req.session.sid

        #js += "<div style=\"background: #ffd;border: 1px solid gray;width:1200px;position: relative;margin-left:auto;margin-right:auto;\">"
        #js += "<br />"

        js += web_utils.web_b(gl_Ower.decode('GBK'))
        js += web_utils.web_input( req,  gl_Ower, 'text', sid, size='10')
        js += web_utils.web_b(gl_LogelFile.decode('GBK'))
        js += web_utils.web_input ( req,  gl_LogelFile, 'text', gl_Win_LogPath+sid, size='40')   
        js += web_utils.web_b(gl_ParseLibs.decode('GBK'))
        js += web_utils.web_input ( req,  gl_ParseLibs, 'text', gl_Win_LogPath+sid, size='40')    
        js += web_utils.web_button(gl_Load, gl_Load)    
        js += "<br /><br />"  

        #js += "</div>"    
         
        return js

def ilog_logtable(env, req, querylist, cur_id):   
        sid = req.session.sid 
        
        out_string = '' 
        rows = []

        if querylist is not None:
            querycount = len(list(querylist))
            if querycount > 0:
                cols = gl_ilog_fields #["id","LogelFile", "ParseLibs", "Status"]
                #out_string+= u'<b>Query Manager: '+str(querycount)+' rows</b>'   
                out_string+= u'<table  id="large" name="large" class="buglisting" style="word-wrap:break-word;word-break:break-all;"   cellspacing="0" cellpadding="0" border="1">'                
                out_string+= u"<tr>"
                for col in cols:            
                    #out_string+= u"<td style='background: #f7f7f0;' width=\"250\">"+col+u"</td>"  
                    out_string+= u"<td style='background: #f7f7f0;' >"+col+u"</td>" 
                out_string+= u"</tr>"   

                for q_row in querylist:
                    rows.append((q_row['ID'],\
                                q_row['Ower'],\
                                q_row['LogelFile'],\
                                q_row['ParseLibs'],\
                                q_row['Status'],\
                                'Del'))
                                
                for row in rows: 
                        out_string+= u"<tr>"
                        index = 0 
                        id = ''
                        for col in row: 
                            if col is None or col == ' ' or col == '':
                                col = '--'    
                                
                            if index == 0:  #name   
                                id = col
                                out_string+= u"<td>" + col + u"</td>"
                            elif index == 4:  #status                                    
                                out_string+= u"<td><a href=\""+gl_base_url+"/"+id+"\" target=\"_blank\">"+ col +u"</a></td>"
                            elif index == 5:  #action   
                                out_string+= u"<td><a href=\""+gl_base_url+"\" onclick=\"javascript:ilog_DelALog(\'"+id+"\');\">Del</a></td>"                                
                            else:
                                out_string+= u"<td>" + col + u"</td>"
                            index = index + 1 
                            
                        out_string+= u"</tr>" 
                        
                out_string+= u"</table>"   
        
        return out_string.encode('utf-8') 
        

def ilog_orignal_data(anntxt, cvs_file1, cvs_file2, cur_id):
    js = ''
    
    js += web_utils.web_b("LogDataFile".decode('GBK'))
    js += web_utils.web_b("ID".decode('GBK'))+str(cur_id)
    js += web_utils.web_b("(1)RSCP CSV".decode('GBK'))+cvs_file1
    js += web_utils.web_b("(2)CellResel CSV".decode('GBK'))+cvs_file2
    js += web_utils.web_b("(3)CellResel Txt".decode('GBK'))+anntxt
    js += "<br />"
    #js += web_input('rpcserver', 'text', '')    

    js += ilog_firefox()
        
    #js += 'LogDataFile: (1)RSCP CSV: '+cvs_file1+' (2)CellResel CSV: '+cvs_file2+' (3)CellResel Txt: '+anntxt
    return js 

def ilog_firefox(): 
        js = '' 
        #js += web_utils.web_b("如果图形不能显示，请用firefox!".decode('GBK'))    
        firefox = "如果图形不能显示，请用firefox!".decode('GBK')
        js += "	<a href='" + gl_firefox_url +  "' target='_blank'>" + firefox + u"</a>"
        #js += "<br />"
    
        return js

def ilog_actionhelp():
#<em><span style="color:#B4009E;">Standard Zoom:</span></em> click and drag, 
#<em><span style="color:#B4009E;">Restore Standard Zoom Level:</span></em> alt and double-click or press button<br>
#<em><span style="color:#B4009E;">Zoom:</span></em> scroll wheel, 
#<em><span style="color:#B4009E;">Pan:</span></em> shift-click and drag, 
#<em><span style="color:#B4009E;">Unzoom: </span></em> double-click
    #js = ''
    help_str = "操作说明: (1)click and drag(放大时间轴) (2)alt-click and drag(拖动时间轴) (3)double-click(初始化) (4)Restore position(返回上次操作状态)".decode('GBK')
    #js += web_utils.web_b(help_str)
    return help_str 

