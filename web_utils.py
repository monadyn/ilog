# -*- coding: utf-8 -*-
#
      
def web_input( req,  name, input_type, default, size='40', readonly=False):
        if default is None or default == '':
            for k, v in req.args.iteritems():
                if k==name and v:
                    default = v
                    
        if readonly == False:
            js = "	<input id=\""+name+"\" type=\""+input_type+"\" name=\""+name+"\" size=\""+size+"\" value=\""+default+"\" />"
        else:
            js = "	<input id=\""+name+"\" type=\""+input_type+"\" name=\""+name+"\" size=\""+size+"\" value=\""+default+"\" readonly=\"1\"/></td>"

        return js  



def web_td_single_sel(name, id, value):				
        js = ''    
        js += "<td>"+name+"</td>"
        js += "<td>"
        js += web_single_sel(value, id) 
        js += "</td>" 
        return js         
    
def web_p(data):
        js = ''    
        js += "<p>"+data+"</p>"
        return js 
        
def web_span(data):
        js = ''    
        js += "<span>"+data+"</span>"
        return js 

def web_b(name):
        js = ''    
        #js += "	<b style=\"background: #eee\">"+name+":</b>"
        js += "	<b>"+name+":</b>"
        return js 
        

        
def web_radio(name, value, checked):
        js = ''  

        #<input type="radio" id="Data_Type" name="Data_Type" value="Percent" checked="checked" />Percent    
        #<input type="radio" id="Data_Type" name="Data_Type" value="Hours" />Hours
        #js += "	<b style=\"background: #eee\">"+id+":</b>"
        if checked == True:
            js += "	<input id=\""+name+"\" type=\"radio\" name=\""+name+"\" value=\""+value+"\" checked=\"checked\" />"+value
        else:
            js += "	<input id=\""+name+"\" type=\"radio\" name=\""+name+"\" value=\""+value+"\" />"+value
                        
        return js 


def web_button(name, value):
        js = ''
        js += "<input type=\"submit\" name=\""+name+"\" value=\""+value+"\" />"   

        return js   

def web_mutls_sel(mutls_sel, name):
        js = ''
  
        js += "<span class=\"field_label  required\"    id=\""+name+"\">"
        js += "<b style=\"background: #eee\">"+name+":</b>"
        js += "</span>"
        js += "      <select name=\""+name+"\" id=\""+name+"\"  multiple=\"multiple\" size=\"7\">"
        nums = 0
        while len(mutls_sel) > nums:
            x = mutls_sel[nums].decode('utf-8')            
            nums += 1  
            js += "                        <option value=\""+x+"\">"+x+"</option>"            
        js += "      </select>"
        return js    

def web_single_sel(sels, name, onclick=''):
        js = ''
        if onclick == '':
            js += "<select name=\""+name+"\" id=\""+name+"\"  >"
        else:
            js += "<select name=\""+name+"\" id=\""+name+"\"  onclick=\""+onclick+"();\">"
        nums = 0
        while len(sels) > nums:
            x = sels[nums].decode('utf-8')            
            nums += 1  
            js += "                        <option value=\""+x+"\">"+x+"</option>"            
        js += "      </select>"
        return js  

def web_li(id, infos):  
        if infos is None or infos == '':
            infos = '---' 
        js = ''    
        #js += "<li>"        
        js += "<strong>"+id+":</strong>"+infos+"      "
        #js += "</li>"
        return js      

def tree_add_node(id, father_id, name, name_id, is_null=False): 
        js = ''
        js += '\r\nd.add('
        js += '\''+id+'\''
        js += ',\''+father_id+'\''
        if is_null == True:
            js += ',\''+name+'\',\"javascript:test();\");'   
        else:
            js += ',\''+name+'\',\"javascript:Bz_QueryTree(\''+name_id+'\',\''+name+'\');\");'      
        return js
  
