# -*- coding: utf-8 -*-
#
import os
import sys  
import csv  
 
#SourcePath = 'C:\\20120312'  
#FileName = 'info.csv'  
  
def csv_action_read(csv_file):   
    result = []
    file_csv = os.path.join(csv_file) 
    csvFile = csv.reader(open(file_csv))  
    for line in csvFile: 
        line_str = ''
        data_len = len(line)
        data_no = 0
        for data in line:  
            data_no += 1
            if data_no < data_len:
                line_str += data.encode('GBK')+','            
            else:
                line_str += data.encode('GBK')

        result.append(line_str)
    return  result
