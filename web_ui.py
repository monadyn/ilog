# -*- coding: utf-8 -*-
#
import os
import pkg_resources
import re
import shutil
import subprocess
import MySQLdb
import time
import thread
from datetime import datetime, timedelta
from genshi import HTML    
from genshi.builder import tag
from trac.util.text import empty
from api import IModuleProvider
from trac.core import *
from trac.loader import get_plugin_info, get_plugins_dir
from trac.perm import PermissionSystem, IPermissionRequestor
from trac.util.compat import partial
from trac.util.text import exception_to_unicode
from trac.util.translation import _, ngettext
from trac.web import HTTPNotFound, IRequestHandler
from trac.web.chrome import add_notice, add_stylesheet, \
                            add_warning, Chrome, INavigationContributor, \
                            ITemplateProvider
from trac.wiki.formatter import format_to_html
from trac.web.chrome import add_ctxtnav,INavigationContributor, ITemplateProvider, add_script
from trac.util import Markup

# stdlib imports
import re
import time

# trac imports
import trac
from trac.core import *
from trac.util.html import html
from trac.web import IRequestHandler
from trac.web.chrome import INavigationContributor, ITemplateProvider
from trac.web.chrome import add_ctxtnav, add_stylesheet, add_script

#iLog
import dygraphs_model
import ilog_model
import utils
import ilog_action
import ilog_web_ui
import ilog_gl

db_conn=None
current_User= ''
Host_Server=1

if Host_Server==0:
    SiteRoot='/trac/ilog'
else:
    SiteRoot='/ilog'
            
class AdminModule(Component):
    """Web administration interface provider and panel manager."""

    implements(INavigationContributor, IRequestHandler, ITemplateProvider)

    module_providers = ExtensionPoint(IModuleProvider)

    # INavigationContributor methods

    def get_active_navigation_item(self, req):
        return 'ilog'

    def get_navigation_items(self, req):
        # The 'Admin' navigation item is only visible if at least one
        #yield 'mainnav', 'ilog', tag.a(_('iLog'), href=req.href('/ilog/report/home'), title=_('report'))
        yield 'mainnav', 'ilog', tag.a(_('iLog'), href=req.href('/ilog/RRC_CellReselcetion/log'), title=_('ilog'))
        #yield 'mainnav', 'ilog', tag.a(_('iLog'), href=req.href('/ilog/RRC_CellReselcetion/log'), title=_('report'))
        
    # IRequestHandler methods

    def match_request(self, req):
        #self.log.debug('----------------------'+req.path_info)
        match = re.match('/ilog(?:/([^/]+)(?:/([^/]+)(?:/(.+))?)?)?$', req.path_info)
        if match:
            req.args['cat_id'] = match.group(1)
            req.args['panel_id'] = match.group(2)
            req.args['path_info'] = match.group(3)
            return True

    def process_request(self, req):
        global db_conn
        global Current_User
        
        Current_User = req.authname.lower()
        if req.authname.lower()=='anonymous':
            add_warning(req, 'Please login first!')
            data = {}
            return 'iLog_Blank.html', data, None 

            
        database_host= self.env.config.get('iManage', 'database_host').strip()
        database_port= self.env.config.getint('iManage', 'database_port')
        database_user= self.env.config.get('iManage', 'database_user').strip()
        database_passwd= self.env.config.get('iManage', 'database_passwd').strip()
        database_db= self.env.config.get('iManage', 'database_db').strip()
        database_charset= self.env.config.get('iManage', 'database_charset').strip()

        import ilog_model
        #db_conn=MySQLdb.connect(host=database_host,port=database_port,user=database_user, passwd=database_passwd, db=database_db,charset=database_charset)
        db_conn=ilog_model.sqldb()
        
        providers = self._get_providers(req)
        cat_id = req.args.get('cat_id') 
        panel_id = req.args.get('panel_id')
        path_info = req.args.get('path_info')
        provider = providers.get((cat_id, panel_id), None)
        
        if not provider:
            raise HTTPNotFound(_('Unknown administration module'))

        template, data = provider.render_module(req, cat_id, panel_id, path_info)

        data.update({
                'active_cat': cat_id, 
                'active_panel': panel_id,
                'SiteRoot':SiteRoot, 
                'CurrentUser':Current_User, 
            })

        return template, data, None

    # ITemplateProvider methods

    def get_htdocs_dirs(self):
        """Return a list of directories with static resources (such as style
        sheets, images, etc.)

        Each item in the list must be a `(prefix, abspath)` tuple. The
        `prefix` part defines the path in the URL that requests to these
        resources are prefixed with.

        The `abspath` is the absolute path to the directory containing the
        resources on the local file system.
        """
        from pkg_resources import resource_filename
        return [('hw', resource_filename(__name__, 'htdocs'))]

    def get_templates_dirs(self):
        from pkg_resources import resource_filename
        return [resource_filename(__name__, 'templates')]

    def _get_providers(self, req):
        """Return a list of available admin panels."""
        providers = {}

        for provider in self.module_providers:
            p = list(provider.get_module(req) or [])
            for panel in p:
                providers[(panel[0], panel[1])] = provider
                
        return providers

class ilog_Script_Module(Component):

    implements(IModuleProvider)

    def get_module(self, req):
        add_ctxtnav(req, _('Script'), href=req.href('/ilog/Script/log'))
        yield ('Script','log')
        
    def render_module(self, req, cat, page, path_info):        
        data = {}         
        a_table = ilog_model.iLog(None, db_conn)  
        
        LogelFile = req.args.get('LogelFile', '').strip() 
        ParseLibs = req.args.get('ParseLibs', '').strip() 
        
        if ilog_gl.gl_Logel2Xml in req.args:  
            self.log.error('ilog_Script_Module: Logel2Xml')
            ilog_action.Script_logel2xml(self.env, req, a_table)    
        
        templates='iLog_Script.html'
        return templates, data         


class ilog_rrc_cell_reselcetion_Module(Component):

    implements(IModuleProvider)

    def get_module(self, req):
        add_ctxtnav(req, _('RRC_CellReselcetion'), href=req.href('/ilog/RRC_CellReselcetion/log'))
        yield ('RRC_CellReselcetion','log')
        
    def render_module(self, req, cat, page, path_info):  
        add_script(req, 'hw/js/iLog_dygraph-combined.js')
        add_script(req, 'hw/js/iLog_interaction.js')
        add_script(req, 'hw/js/iLog_excanvas.js')   
        add_script(req, 'hw/js/iLog_ajax.js')  
        #add_script(req, 'hw/js/iLog_rrc_cell_reselcetion.js')          
        data = {}        
        a_table = ilog_model.iLog(None, db_conn)    
        status = 'ready'
        
        if ilog_web_ui.gl_Load in req.args: 
            ilog_action.ilog_load(self.env, req, a_table)            

        ajax = req.args.get('ajax', '')
        self.log.error('ilog_rrc_cell_reselcetion_Module: ajax=(%s)', ajax)
        if ajax == 'del':            
            ID = req.args.get('ID', '').strip()                    
            if ID:
                b_table = ilog_model.iLog(ID, db_conn)
                self.log.error('ilog_rrc_cell_reselcetion_Module: ajax del(%s)', ID)
                ilog_action.ilog_delete(self.env, req, b_table, ID)    
        
        if path_info is None:
            cur_id = 'Sample'            
            js = ilog_web_ui.ilog_rrc_cell_reselcetion(self.env, req, db_conn)
            QF = {}
            #QF['field_Ower'] = req.session.sid
            querylist = a_table.select(QF) 
            if len(querylist) > 0: 
                js += ilog_web_ui.ilog_logtable(self.env, req, querylist, '')             
            data.update({'log_manager':HTML(js)})   
        else:
            cur_id = path_info
            b_table = ilog_model.iLog(cur_id, db_conn)
            status = b_table['Status']
            
        if status == 'loading':    
            add_notice(req,'@@@Log file(%s) is not ready!Plz,Check!@@@',cur_id)
            cur_id = 'Sample'
        
        anntxt = dygraphs_model.DyGraph.get_logdata_base_url()+cur_id+'/'+cur_id+'.txt'
        dygraph_data1 = dygraphs_model.DyGraph.get_logdata_base_url()+cur_id+'/'+cur_id+'.csv'
        dygraph_data2 = ''
        
        js = ilog_web_ui.ilog_orignal_data(anntxt, dygraph_data1, dygraph_data2, cur_id)
        data.update({'orignal_data':HTML(js)}) 
        
        #dygraph_data1 = dygraphs_model.DyGraph.get_dygraph_data(cvs_file1)        
        #dygraph_data2 = dygraphs_model.DyGraph.get_dygraph_data2(cvs_file2)        
        dygraph_js = dygraphs_model.DyGraph.get_cell_resel_dygraph_js(anntxt, dygraph_data1, dygraph_data2)        
        data.update({'dygraph_js':dygraph_js}) 
        
        #help_connent_str = ilog_web_ui.ilog_firefox()         
        help_connent_str = ilog_web_ui.ilog_actionhelp()
        data.update({'help_connent':help_connent_str})
        
        templates='iLog_rrc_cell_reselcetion.html'
        
        #return templates, {'worklog_report': data}
        return templates, data



class ilog_Debug_Module(Component):

    implements(IModuleProvider)

    def get_module(self, req):
        add_ctxtnav(req, _('Debug'), href=req.href('/ilog/Debug/log'))
        yield ('Debug','log')
        
    def render_module(self, req, cat, page, path_info):
        add_script(req, 'hw/js/iLog_dygraph-combined.js')
        add_script(req, 'hw/js/iLog_interaction.js')
        add_script(req, 'hw/js/iLog_Debug.js')     
        add_stylesheet(req, 'hw/css/iLog_Debug.css')   
        
        #add_script(req, 'hw/js/excanvas.js')
        data = {}         
        data['url'] = 'http://tracsrv/trac/ilog/chrome/site/Log/Sample/Sample.txt'
        data.update({'url':'http://tracsrv/trac/ilog/chrome/site/Log/Sample/Sample.txt'})
        templates='iLog_Debug.html'
        data['comments'] = "http://tracsrv/chrome/site/Log/Sample/Sample.csv"
        #return templates, {'worklog_report': data}
        return templates, data  
        
class ilog_Demo_Module(Component):

    implements(IModuleProvider)

    def get_module(self, req):
        add_ctxtnav(req, _('Demo'), href=req.href('/ilog/Demo/log'))
        yield ('Demo','log')
        
    def render_module(self, req, cat, page, path_info):
        add_script(req, 'site/Log/TangWeiDemo/dygraph-combined.js')
        add_script(req, 'site/Log/TangWeiDemo/interaction.js')
        add_script(req, 'site/Log/TangWeiDemo/excanvas.js')
        
        add_script(req, 'site/Log/TangWeiDemo/script_body.js')
        add_script(req, 'site/Log/TangWeiDemo/cell_info.js')         
        add_script(req, 'site/Log/TangWeiDemo/serv_cell_change.js')
        add_script(req, 'site/Log/TangWeiDemo/reselection.js')

        add_stylesheet(req, 'hw/css/cssStyle.css')   
        
        data = {}         
        templates='iLog_Demo.html'
        return templates, data 

class ilog_Help_Module(Component):

    implements(IModuleProvider)

    def get_module(self, req):
        add_ctxtnav(req, _('Help'), href=req.href('/ilog/help/help'))
        yield ('help','help')

    def render_module(self, req, cat, page, path_info):
        data = {}
        
        templates='worklog_help.html'
        
        return templates, {'worklog_report': data}
 



def fmt_timestamp(seconds):
    millis = int(seconds * 1000) % 1000
    localtime = time.localtime(seconds)
    text = []
    text.append(time.strftime('%m/%d/%y  %H:%M:%S', localtime))
    text.append('.%03d' % millis)
    return "".join(text)


class TracSqlPlugin(Component):
    implements(INavigationContributor, IRequestHandler, ITemplateProvider)

    # INavigationContributor methods

    def get_active_navigation_item(self, req):
        return 'Build CR'

    def get_navigation_items(self, req):
        if not req.perm.has_permission('TRAC_ADMIN'):
            return
        yield 'mainnav', 'sql', html.A('SQL', href=req.href.sql())

    # ITemplateProvider methods

    def get_htdocs_dirs(self):
        from pkg_resources import resource_filename
        return [('sql', resource_filename(__name__, 'htdocs'))]

    def get_templates_dirs(self):
        from pkg_resources import resource_filename
        return [resource_filename(__name__, 'templates')]

    # IRequestHandler methods

    def match_request(self, req):
        import re
        match = re.match(r'/sql(?:(/.*))?', req.path_info)
        if match:
            path, = match.groups()
            req.args['path'] = path or '/'
            return True

	#tag main
    def process_request(self, req):
        req.perm.require('TRAC_ADMIN')

        path = req.args.get('path', '')

        data = {}

        db = self.env.get_db_cnx()
        cursor = db.cursor()

        db_str = self.env.config.get('trac', 'database')
        db_type, db_path = db_str.split(':', 1)
        assert db_type in ('sqlite', 'mysql', 'postgres'), \
                            'Unsupported database "%s"' % db_type
        self.db_type = db_type

        # Include trac wiki stylesheet
        add_stylesheet(req, 'common/css/wiki.css')

        # Include trac stats stylesheet
        add_stylesheet(req, 'sql/common.css')

        # Include javascript libraries
        add_script(req, 'stats/jquery-1.4.2.min.js')

        # Include context navigation links
        self.log.debug('cuilian: process_request')
        add_ctxtnav(req, 'sql', req.href.sql())
        add_ctxtnav(req, 'Schema', req.href.sql('schema'))

        if path == '/':
            result = self._process_build_ueit(req, cursor, data)
            cursor.close()
            return result

        elif path == '/schema':
            result = self._process_schema(req, cursor, data)
            cursor.close()
            return result

        else:
            cursor.close()
            raise ValueError, "unknown path '%s'" % path

    def _process_build_ueit(self, req, cursor, data):

        if self.db_type == 'mysql':
            cursor.execute('set SQL_SELECT_LIMIT=1000')
        else:
            pass

        sql = req.args.get('query', '')
        raw = req.args.get('raw', '')
        csv = req.args.get('csv', '')

        cols = rows = []
        took = 0
        error = None

        if re.search('.*delete|drop|insert|replace|set|update.*', sql,
                     re.IGNORECASE):
            error = "Query must be read-only! use selct cmd!"

        elif sql.strip():
            try:
                start = time.time()
                cursor.execute(sql)
                cols = map(lambda x: x[0], cursor.description)
                rows = cursor.fetchall()[:1000]
                took = '%.3f' % (time.time() - start)
            except BaseException, e:
                error = e.message
            except e:
                error = e

        if csv:
            text = []
            for col in cols:
                text.append('%s,' % col)
            text.append('\n')
            for row in rows:
                for cell in row:
                    text.append("%s," % cell)
                text.append('\n')
            text = str(''.join(text))
            req.send_response(200)
            req.send_header('Content-Type', 'text/csv')
            req.send_header('Content-Length', str(len(text)))
            req.end_headers()
            req.write(text)
            return

        if not raw:

            format = {
                'path' : lambda x: html.A(x, href=req.href.browser(x)),
                'rev' : lambda x: html.A(x, href=req.href.changeset(x)),
                'ticket' : lambda x: html.A(x, href=req.href.ticket(x)),
                'query' : lambda x: html.PRE(x, style="padding: 0; margin: 0;"),
                'time' : lambda x: fmt_timestamp(x),
            }

            if trac.__version__.startswith('0.12'):
                format['time'] = lambda x: fmt_timestamp(x/1000000.)

            format['base_path'] = format['path']
            format['base_rev'] = format['rev']
            format['changetime'] = format['time']

            def format_wiki_text(text):
                from trac.mimeview.api import Mimeview
                mimeview = Mimeview(self.env)
                mimetype = 'text/x-trac-wiki'
                return mimeview.render(req, mimetype, text)

            if re.search('.*from wiki.*', sql, re.IGNORECASE|re.MULTILINE):
                format['name'] = lambda x: html.A(x, href=req.href.wiki(x))
                format['text'] = format_wiki_text
            elif re.search('.*from ticket.*', sql, re.IGNORECASE|re.MULTILINE):
                format['id'] = lambda x: html.A(x, href=req.href.ticket(x))
                format['component'] = lambda x: html.A(x, href=req.href.query(component=x))
                format['severity'] = lambda x: html.A(x, href=req.href.query(severity=x))
                format['type'] = lambda x: html.A(x, href=req.href.query(type=x))
                format['milestone'] = lambda x: html.A(x, href=req.href.query(milestone=x))
                format['version'] = lambda x: html.A(x, href=req.href.query(version=x))
                format['status'] = lambda x: html.A(x, href=req.href.query(status=x))
                format['owner'] = lambda x: html.A(x, href=req.href.query(owner=x))
                format['reporter'] = lambda x: html.A(x, href=req.href.query(reporter=x))
                format['priority'] = lambda x: html.A(x, href=req.href.query(priority=x))
                format['resolution'] = lambda x: html.A(x, href=req.href.query(resolution=x))
            elif re.search('.*from report.*', sql, re.IGNORECASE|re.MULTILINE):
                format['id'] = lambda x: html.A(x, href=req.href.report(x))

            default = lambda x: x

            formats = [format.get(col, default) for col in cols]
            for i, row in enumerate(rows):
                rows[i] = [fmt(col) for fmt, col in zip(formats, row)]

        data['query'] = sql
        data['error'] = error
        data['cols'] = cols
        data['rows'] = rows
        data['took'] = took
        data['raw'] = raw

        return 'sql.html', data, None

    def _process_schema(self, req, cursor, data):

        if self.db_type == 'mysql':
            sql = 'show tables'
        elif self.db_type == 'sqlite':
            sql = 'SELECT name FROM sqlite_master WHERE type = "table"'
        elif self.db_type == 'postgres':
            sql = "select table_name from information_schema.tables where table_schema = 'public'"
        else:
            assert False, "Unsupported db_type: %s" % self.db_type

        cursor.execute(sql)
        rows = cursor.fetchall()

        table = req.args.get('table', '')
        valid = False

        tables = []
        for x, in sorted(rows):
            if x == table:
                valid = True
                tables.append(html.B(x))
            else:
                tables.append(html.A(x, href=req.href.sql('schema', table=x)))

        if table and valid:
            cols = ["name", "type", "nullable", "default"]
            if self.db_type == 'mysql':
                sql = 'describe %s' % table
                cursor.execute(sql)
                results = cursor.fetchall()
                rows = []
                for field, type, null, key, default, extra in results:
                    rows.append((field, type, null, default))
            elif self.db_type == 'sqlite':
                sql = 'PRAGMA table_info("%s")' % table
                cursor.execute(sql)
                results = cursor.fetchall()
                rows = []
                for cid, name, type, notnull, dflt_value, pk in results:
                    rows.append((name, type, notnull, dflt_value))
            elif self.db_type == 'postgres':
                sql = """
                select
                  column_name,
                  data_type,
                  is_nullable,
                  column_default
                from information_schema.columns
                where table_schema = 'public' and
                      table_name = '%s'
                """ % table
                cursor.execute(sql)
                rows = cursor.fetchall()
            else:
                assert False, "Unsupported db_type: %s" % self.db_type


            cursor.execute("select count(*) from %s" % table)
            count, = cursor.fetchall()[0]

            if self.db_type == 'mysql':
                sql = """
                select
                    index_name,
                    group_concat(column_name
                                 order by seq_in_index
                                 separator ", ")
                from information_schema.statistics
                where table_name = '%s'
                group by index_name
                """ % table
                cursor.execute(sql)
                indexes = cursor.fetchall()
            elif self.db_type == 'sqlite':
                sql = """
                select name
                from sqlite_master
                where tbl_name = '%s'
                  and type = 'index'
                """ % table
                cursor.execute(sql)
                results = cursor.fetchall()
                indexes = []
                for index, in results:
                    cursor.execute("PRAGMA index_info('%s')" % index)
                    indexes.append((index, ", ".join(name for _, _, name in
                                                     cursor.fetchall())))
            elif self.db_type == 'postgres':
                sql = "select indexname, indexdef from pg_indexes where tablename = '%s'" % table
                cursor.execute(sql)
                indexes = cursor.fetchall()
            else:
                assert False, "Unsupported db_type: %s" % self.db_type

        else:
            cols = rows = []
            count = 0
            indexes = []

        data['tables'] = tables
        data['table'] = table
        data['cols'] = cols
        data['rows'] = rows
        data['count'] = count
        data['indexes'] = indexes

        # FIXME: Add foreign key list?

        return 'schema.html', data, None


