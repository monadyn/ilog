# -*- coding: utf-8 -*-
#
import os
import sys 
import shutil

from trac.web.chrome import add_notice

import ilog_web_ui
import utils
import ilog_gl


def  ilog_delete(env, req, a_table, ID) :
    #shutil.rmtree(ilog_web_ui.gl_LogPath+ID)
    utils._del_path(ilog_web_ui.gl_LogPath+ID)
    a_table.delete()
    return
   

def  ilog_validate(env, req):    
    logelfile = req.args.get(ilog_web_ui.gl_LogelFile)
    parselibs = req.args.get(ilog_web_ui.gl_ParseLibs) 
    
    if utils._check_file(req, logelfile) \
                and utils._check_dir(req, parselibs):
        if utils._check_file(req, os.path.join(parselibs, 'mtcdbginfo.dll')) \
                    and utils._check_file(req, os.path.join(parselibs, 'mtcoffset.dat')) \
                    and utils._check_file(req, os.path.join(parselibs, 'ttgui.exe')):        
            return True
            
    return False
    
    
def  ilog_load(env, req, a_table):
    ower = req.args.get(ilog_web_ui.gl_Ower) 
    logelfile = req.args.get(ilog_web_ui.gl_LogelFile)
    parselibs = req.args.get(ilog_web_ui.gl_ParseLibs)                       

    if ilog_validate(env, req):
        pass
    else:
        return
        
    a_table['Ower'] = ower
    a_table['LogelFile'] = logelfile
    a_table['ParseLibs'] = parselibs #os.path.join(log_path,cvs_file2)
    a_table['Status'] = 'loading'
    a_table.insert()

    curid = a_table.ID
    
    #D:\trac\ilog\htdocs\Log
    os.mkdir(ilog_web_ui.gl_LogPath+curid)
    os.mkdir(ilog_web_ui.gl_LogPath+curid+'\\tmp')
    xml_file = logel2xml(env, logelfile, parselibs, curid)
    tag_file = xml2tag(env, xml_file, curid)
    tag2csv(env, tag_file, curid)
    #shutil.rmtree(ilog_web_ui.gl_LogPath+curid+'\\tmp')
    utils._del_path(ilog_web_ui.gl_LogPath+curid+'\\tmp')
    env.log.error('ilog_load,ok')
              
    a_table['Status'] = 'ready'
    a_table.save_changes()    
    return
    
def logel2xml(env, logelfile, parselibs, curid):   
#"D:\Log\Tools\logel2xml\logel2txt.exe" --logel "D:\Log\Tools\logel2xml\arm.logel" --filter "D:\Log\Tools\logel2xml\a.filter" --dbdir "D:\Log\Tools\logel2xml\db" --output "D:\Log\Tools\logel2xml\a.xml"

    #env.log.error('logel2xml,%s,%s,%s',logelfile, parselibs, curid)
    xml_file = ilog_web_ui.gl_LogPath+curid+'\\tmp'+'\\'+curid+'.xml'
    cmd = ilog_web_ui.gl_LogToolsPath+'logel2xml\\logel2txt.exe'
    cmd += '  --filter '+'\"'+ilog_web_ui.gl_LogToolsPath+'a.filter\"'
    cmd += '  --logel '+'\"'+logelfile+'\"'
    cmd += '  --dbdir '+'\"'+parselibs+'\"'
    cmd += '  --output '+'\"'+xml_file+'\"'
    env.log.error('logel2xml,cmd=%s',cmd)
    ret = os.system(cmd)
    env.log.error('logel2xml,ret=%s',ret)
    return  xml_file

def xml2tag(env, xml_file, curid): 
#perl td_log_analyze.pl "D:\Log\LAND-LOG PLAYER\a.xml" data.tag
#D:\Log\Tools\xml2csv_2\xml2summary.pl    D:\trac\ilog\htdocs\Log\22\tmp\22.xml   D:\trac\ilog\htdocs\Log\22\tmp\22.tag

    tag_file = xml_file.replace('.xml','.tag')   
    #cmd = 'pushd  '+ilog_web_ui.gl_LogToolsPath+'xml2csv\n\n'
    #C:\Perl\site\lib
    #cmd = 'perl '+ ilog_web_ui.gl_LogToolsPath+'xml2csv\\td_log_analyze.pl '
    cmd = 'perl '+ ilog_web_ui.gl_LogToolsPath+'xml2csv\\xml2summary.pl '
    
    cmd += '   '+xml_file
    cmd += '   '+tag_file
    env.log.error('xml2tag,cmd=%s',cmd)
    os.system(cmd)
    
    return  tag_file    

def tag2csv(env, tag_file, curid):   
#D:\Log\PerlScript>perl td_log_analyze.pl data.tag data.csv ann.txt annfile.csv
#perl td_log_analy.pl test.tag data.csv ann.txt annfile.csv
#perl D:\Log\Tools\xml2csv_2\td_log_analyze.pl    D:\trac\ilog\htdocs\Log\22\tmp\22.tag   D:\trac\ilog\htdocs\Log\22\22.csv   D:\trac\ilog\htdocs\Log\22\22.txt
    txt_file = ilog_web_ui.gl_LogPath+curid+ '\\'+curid+'.txt'
    csv_file = ilog_web_ui.gl_LogPath+curid+ '\\'+curid+'.csv'
    #csv2_file = csv_file.replace('.csv','_0.csv')
    #cmd = 'pushd  '+ilog_web_ui.gl_LogToolsPath+'xml2csv\n\n'    
    cmd = 'perl '+ ilog_web_ui.gl_LogToolsPath+'xml2csv\\td_log_analyze.pl '
    cmd += '   '+tag_file
    cmd += '   '+csv_file     
    cmd += '   '+txt_file   
    #cmd += '   '+csv2_file     
    env.log.error('tag2csv,cmd=%s',cmd)
    os.system(cmd)
    
    return     


   
def Script_logel2xml(env, req, a_table):   
    ower = req.args.get(ilog_web_ui.gl_Ower) 
    logelfile = req.args.get(ilog_web_ui.gl_LogelFile)
    parselibs = req.args.get(ilog_web_ui.gl_ParseLibs)                       

    if ilog_validate(env, req):
        pass
    else:
        return
        
    a_table['Ower'] = ower
    a_table['LogelFile'] = logelfile
    a_table['ParseLibs'] = parselibs #os.path.join(log_path,cvs_file2)
    a_table['Status'] = 'loading'
    a_table.insert()

    curid = a_table.ID
    
    #D:\trac\ilog\htdocs\Log
    os.mkdir(ilog_gl.gl_iLog+curid)
    os.mkdir(ilog_gl.gl_iLog+curid+'\\tmp')

#"D:\Log\Tools\logel2xml\logel2txt.exe" --logel "D:\Log\Tools\logel2xml\arm.logel" --filter "D:\Log\Tools\logel2xml\a.filter" --dbdir "D:\Log\Tools\logel2xml\db" --output "D:\Log\Tools\logel2xml\a.xml"

    #env.log.error('logel2xml,%s,%s,%s',logelfile, parselibs, curid)
    xml_file = ilog_gl.gl_iLog+curid+'\\tmp'+'\\'+curid+'.xml'
    cmd = ilog_web_ui.gl_LogToolsPath+'logel2xml\\logel2txt.exe'
    #cmd += '  --filter '+'\"'+ilog_web_ui.gl_LogToolsPath+'a.filter\"'
    cmd += '  --logel '+'\"'+logelfile+'\"'
    cmd += '  --dbdir '+'\"'+parselibs+'\"'
    cmd += '  --output '+'\"'+xml_file+'\"'
    env.log.error('Script_logel2xml,cmd=%s',cmd)
    ret = os.system(cmd)
    env.log.error('Script_logel2xml,ret=%s',ret)
    return  xml_file